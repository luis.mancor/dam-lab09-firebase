require('firebase/firestore');
import firebase from '../firebase';

import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View } from 'react-native';
import { ListItem } from 'react-native-elements'
import { TextInput,Paragraph,Button   } from 'react-native-paper';

//nombre:'',
//paisOrigen:'',
//precio:'',
class UserScreen extends Component {

  constructor() {
    super();
    this.firestoreDB = firebase.firestore().collection('users');
    this.state = {
      isLoading: true,
      arrayPlatos: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreDB.onSnapshot(this.getCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }
  
  //REALIZA LA PETICION DE BUSQUEDA FOR EACH
  getCollection = (querySnapshot) => {
    const arrayPlatos = [];
    querySnapshot.forEach((res) => {
      const { nombre, paisOrigen, precio,imagen } = res.data();
      arrayPlatos.push({
        key: res.id,
        res,
        nombre,
        paisOrigen,
        precio,
        imagen,
      });
    });
    this.setState({
      arrayPlatos,
      isLoading: false,
   });
  }

  render() {
    
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      
      <ScrollView style={styles.container}>
          {
            this.state.arrayPlatos.map((item, i) => {
              var concatenado =`Originario de ${item.paisOrigen} con un precio promedio de S/.${item.precio} ` 
              if(item.imagen){
                var imagenDisplay = item.imagen;
              }else{
                var imagenDisplay = 'https://todomundotecnology.com/panel/img/marcas/otro.png';
              }
              return (
                <ListItem
                  key={i}

                  title={item.nombre}
                  subtitle={concatenado}
                  leftAvatar={{ source: { uri: imagenDisplay } }}
                  bottomDivider
                  chevron={{ color: 'orange' }}
                  onPress={() => {
                    this.props.navigation.navigate('UserDetailScreen', {
                      userkey: item.key
                    });
                  }}/>
              );
            })
          }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingBottom: 22
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default UserScreen;