
require('firebase/firestore');
import firebase from '../firebase';

import React, { Component } from 'react';
import { Alert,  StyleSheet,  ScrollView, ActivityIndicator, View } from 'react-native';
import { TextInput,Paragraph,Button,Headline    } from 'react-native-paper';


class UserDetailScreen extends Component {

  constructor() {
    super();
    this.state = {
      nombre: '',
      paisOrigen: '',
      precio: '',
      imagen:'',
      isLoading: true
    };
  }
 
  componentDidMount() {
    const DB = firebase.firestore().collection('users').doc(this.props.route.params.userkey)
    DB.get().then((res) => {
      if (res.exists) {
        const plato = res.data();
        this.setState({
          key: res.id,
          nombre: plato.nombre,
          paisOrigen: plato.paisOrigen,
          precio: plato.precio,
          imagen: plato.imagen,
          isLoading: false,
        });
      } else {
        console.log("Document does not exist!");
      }
    });
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  updateUser() {
    this.setState({
      isLoading: true,
    });
    const updateDB = firebase.firestore().collection('users').doc(this.state.key);
    updateDB.set({
      nombre: this.state.nombre,
      paisOrigen: this.state.paisOrigen,
      precio: this.state.precio,
      imagen: this.state.imagen,
    }).then((docRef) => {
      this.setState({
        key: '',
        nombre: '',
        paisOrigen: '',
        precio: '',
        imagen:'',
        isLoading: false,
      });
      this.props.navigation.navigate('UserScreen');
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      });
    });
  }

  deleteUser() {
    const DB = firebase.firestore().collection('users').doc(this.props.route.params.userkey)
      DB.delete().then((res) => {
          console.log('Item borrado de la base de datos')
          this.props.navigation.navigate('UserScreen');
      })
  }


  openTwoButtonAlert=()=>{
    Alert.alert(
      'Borrar plato',
      'Esta seguro?',
      [
        {text: 'Si', onPress: () => this.deleteUser()},
        {text: 'No', onPress: () => console.log('No se borro ningun registro.'), style: 'cancel'},
      ],
      { 
        cancelable: true 
      }
    );
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={{margin:20}}>
          <Headline style={{alignSelf:'center', marginBottom:9}}>Editar plato</Headline>
          <Paragraph>Ingrese su nombre</Paragraph>
          <TextInput
          label='Nombre'
          mode='outlined'
          value={this.state.nombre}
          onChangeText={(val) => this.inputValueUpdate(val, 'nombre')}
          />
          <Paragraph>Pais de origen</Paragraph>
          <TextInput
          label='paisOrigen'
          mode='outlined'
          multiline={true}
          numberOfLines={1}
          value={this.state.paisOrigen}
          onChangeText={(val) => this.inputValueUpdate(val, 'paisOrigen')}
          />
          <Paragraph>Precio unitario</Paragraph>
          <TextInput
          label='Precio unitario'
          mode='outlined'
          value={this.state.precio}
          onChangeText={(val) => this.inputValueUpdate(val, 'precio')}
          />
          <Paragraph>Imagen referencial</Paragraph>
          <TextInput
          label='Imagen'
          mode='outlined'
          multiline={true}
          numberOfLines={5}
          value={this.state.imagen}
          onChangeText={(val) => this.inputValueUpdate(val, 'imagen')}
          />
          <Button 
            style={{marginTop:20}}
            icon="check" mode="outlined" onPress={() => this.updateUser()} >
            Modificar
          </Button>
          <Button 
            style={{marginTop:20}}
            icon="delete" mode="contained" onPress={this.openTwoButtonAlert} >
            Eliminar registro
          </Button>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 7, 
  }
})

export default UserDetailScreen;











//https://www.positronx.io/create-react-native-firebase-crud-app-with-firestore/