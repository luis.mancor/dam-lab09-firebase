
require('firebase/firestore');
import firebase from '../firebase';
import React,{Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Image,Alert,ActivityIndicator} from 'react-native';
import { TextInput,Paragraph,Button,Headline    } from 'react-native-paper';
export default class AddUserScreen extends Component{

    constructor(props){
      super(props);
      this.state={
        nombre:'',
        paisOrigen:'',
        precio:'',
        imagen:'',
        count:0,
        isLoading:false,
      }
    }
  
    storeUser(){
      console.log('Click en StoreUser');
      console.log(this.state.nombre);
  
      if(this.state.nombre === '' || this.state.paisOrigen === ''){
        //alert('Los campos son obligatorios');
        Alert.alert(
          'Validacion',
          'Por favor llene los campos requeridos',
          [
            {
              text: 'Cancelar',
              style: 'cancel'
            },
            { text: 'OK', onPress: () => console.log('OK Pressed') }
          ],
          { cancelable: false }  
        );

      }else{
        this.setState({
          isLoading:true,
        });
  
        const db = firebase.firestore();
        db.settings({experimentalForceLongPolling:true});
        db.collection('users').add({
          nombre: this.state.nombre,
          paisOrigen: this.state.paisOrigen,
          precio: this.state.precio,
          imagen: this.state.imagen,
        }).then((res) => {
          this.setState({
            nombre:'',
            paisOrigen:'',
            precio:'',
            imagen:'',
            isLoading:false,
          });
          this.props.navigation.navigate('UserScreen')
        })
        //Fin de else
      }
    }
    
  
    render(){
      return(
        <View style={{margin:20}}>
          <Headline style={{alignSelf:'center', marginBottom:9}}>Registro de Platos</Headline>
          <Paragraph>Ingrese su nombre</Paragraph>
          <TextInput
          label='Nombre'
          mode='outlined'
          value={this.state.nombre}
          onChangeText={nombre => this.setState({ nombre })}
          />
          <Paragraph>Pais de origen</Paragraph>
          <TextInput
          label='paisOrigen'
          mode='outlined'
          value={this.state.paisOrigen}
          onChangeText={paisOrigen => this.setState({ paisOrigen })}
          />
          <Paragraph>Precio unitario</Paragraph>
          <TextInput
          label='Precio'
          mode='outlined'
          value={this.state.precio}
          onChangeText={precio => this.setState({ precio })}
          />
          <Paragraph>Imagen referencial</Paragraph>
          <TextInput
          label='Imagen'
          mode='outlined'
          multiline={true}
          numberOfLines={4}
          value={this.state.imagen}
          onChangeText={imagen => this.setState({ imagen })}
          />
          <Button 
            style={{marginTop:20}}
            icon="checkbox-marked-circle-outline" mode="contained" onPress={() => this.storeUser()}>
            Agregar
          </Button>
        </View>
        
      );
    }
  
  }

