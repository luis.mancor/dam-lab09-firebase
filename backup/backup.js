//adduserScreen


/*
import React, { Component } from 'react';
import { Button, StyleSheet, TextInput, ScrollView, ActivityIndicator, View } from 'react-native';


class AddUserScreen extends Component {
  constructor() {
    super();
    this.dbRef = firebase.firestore().collection('users');
    this.state = {
      name: '',
      email: '',
      mobile: '',
      isLoading: false
    };
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  storeUser() {
    if(this.state.name === ''){
     alert('Fill at least your name!')
    } else {
      this.setState({
        isLoading: true,
      });      
      this.dbRef.add({
        name: this.state.name,
        email: this.state.email,
        mobile: this.state.mobile,
      }).then((res) => {
        this.setState({
          name: '',
          email: '',
          mobile: '',
          isLoading: false,
        });
        this.props.navigation.navigate('UserScreen')
      })
      .catch((err) => {
        console.error("Error found: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.inputGroup}>
          <TextInput
              placeholder={'Name'}
              value={this.state.name}
              onChangeText={(val) => this.inputValueUpdate(val, 'name')}
          />
        </View>
        <View style={styles.inputGroup}>
          <TextInput
              multiline={true}
              numberOfLines={4}
              placeholder={'Email'}
              value={this.state.email}
              onChangeText={(val) => this.inputValueUpdate(val, 'email')}
          />
        </View>
        <View style={styles.inputGroup}>
          <TextInput
              placeholder={'Mobile'}
              value={this.state.mobile}
              onChangeText={(val) => this.inputValueUpdate(val, 'mobile')}
          />
        </View>
        <View style={styles.button}>
          <Button
            title='Add User'
            onPress={() => this.storeUser()} 
            color="#19AC52"
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default AddUserScreen;

*/

/*
Funcion anterior


require('firebase/firestore');
import firebase from '../firebase';
import React,{Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Image,Alert} from 'react-native';
import { TextInput,Paragraph,Button,Headline    } from 'react-native-paper';
export default class AddUserScreen extends Component{

    constructor(props){
      super(props);
      this.state={
        name:'',
        email:'',
        mobile:'',
        count:0,
        isLoading:false,
      }
    }
  
    storeUser(){
      console.log('Click en StoreUser');
      console.log(this.state.name);
  
      if(this.state.name === ''){
        //alert('Llena almenos el nombre!');
        console.log('Llene nombre');
      }else{
        this.setState({
          isLoading:true,
        });
  
        const db = firebase.firestore();
        db.settings({experimentalForceLongPolling:true});
        db.collection('users').add({
          name: this.state.name,
          email: this.state.email,
          mobile: this.state.mobile,
        }).then((res) => {
          this.setState({
            name:'',
            email:'',
            mobile:'',
            isLoading:false,
          });
          this.props.navigation.navigate('UserScreen')
        })
        //Fin de else
      }
    }
    
  
    render(){
      return(
        <View style={{margin:20}}>
          <Headline style={{alignSelf:'center', marginBottom:9}}>Registro de Platos</Headline>
          <Paragraph>Ingrese su nombre</Paragraph>
          <TextInput
          label='Nombre'
          mode='outlined'
          value={this.state.name}
          onChangeText={name => this.setState({ name })}
          />
          <Paragraph>Ingrese su email</Paragraph>
          <TextInput
          label='Email'
          mode='outlined'
          value={this.state.email}
          onChangeText={email => this.setState({ email })}
          />
          <Paragraph>Ingrese su celular</Paragraph>
          <TextInput
          label='celular'
          mode='outlined'
          value={this.state.mobile}
          onChangeText={mobile => this.setState({ mobile })}
          />
          <Button 
            style={{marginTop:20}}
            icon="bell" mode="contained" onPress={() => this.storeUser()}>
            Agregar
          </Button>
        </View>
        
      );
    }
  
  }





*/