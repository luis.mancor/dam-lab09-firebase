/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 * 
 * https://console.firebase.google.com/
 */

/*
Importaciones: (La del firebase esta en el lab)
yarn add react-native-paper
yarn add react-native-vector-icons
react-native link react-native-vector-icons

yarn add @react-navigation/native
yarn add react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view
npm install @react-navigation/stack

yarn add react-native-elements
*/




//import '@firebase/firestore';
require('firebase/firestore');
import firebase from './firebase';
import React,{Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Image,Alert} from 'react-native';
import { TextInput,Paragraph,Button   } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AddUserScreen from './screens/AddUserScreen';
import UserScreen from './screens/UserScreen';
import UserDetailScreen from './screens/UserDetailScreen';

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
          headerStyle: {
            backgroundColor: '#621FF7',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      >
      <Stack.Screen 
        name="AddUserScreen" 
        component={AddUserScreen} 
        options={{ title: 'Agregar Plato' }}
      />
      <Stack.Screen 
        name="UserScreen" 
        component={UserScreen} 
        options={{ title: 'Lista de platos' }}
      />
      <Stack.Screen 
       name="UserDetailScreen" 
       component={UserDetailScreen} 
       options={{ title: 'Detalle de plato' }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

/*
export default class App extends Component{

  constructor(props){
    super(props);
    this.state={
      name:'',
      email:'',
      mobile:'',
      count:0,
      isLoading:false,
    }
  }

  storeUser(){
    console.log('Click en StoreUser');
    console.log(this.state.name);

    if(this.state.name === ''){
      //alert('Llena almenos el nombre!');
      console.log('Llene nombre');
    }else{
      this.setState({
        isLoading:true,
      });

      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling:true});
      db.collection('users').add({
        name: this.state.name,
        email: this.state.email,
        mobile: this.state.mobile,
      }).then((res) => {
        this.setState({
          name:'',
          email:'',
          mobile:'',
          isLoading:false,
        });
      })
      //Fin de else
    }
  }
  

  render(){
    return(
      <View style={{margin:20}}>
        <Paragraph>Ingrese su nombre</Paragraph>
        <TextInput
        label='Nombre'
        mode='outlined'
        value={this.state.name}
        onChangeText={name => this.setState({ name })}
        />
        <Paragraph>Ingrese su email</Paragraph>
        <TextInput
        label='Email'
        mode='outlined'
        value={this.state.email}
        onChangeText={email => this.setState({ email })}
        />
        <Paragraph>Ingrese su celular</Paragraph>
        <TextInput
        label='celular'
        mode='outlined'
        value={this.state.mobile}
        onChangeText={mobile => this.setState({ mobile })}
        />
        <Button 
          style={{marginTop:20}}
          icon="bell" mode="contained" onPress={() => this.storeUser()}>
          Agregar
        </Button>
      </View>
      
    );
  }

}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    paddingHorizontal:10,

  },
  text:{
    alignItems:'center',
    padding:10,
  },

  button:{
    top:10,
    alignItems:'center',
    backgroundColor:'#d7f8ff',
    padding:10,
    borderColor:'#3e4444',    
  },

  countCointainer:{
    alignItems:'center',
    padding:10,
  },
  countText:{
    color:'#6b5b95',
  }

});
*/

