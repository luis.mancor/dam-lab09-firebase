import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyCuKQ8ecvHrKV06k7mGWHL0VYCj8ToDswc",
    authDomain: "proyecto09-16468.firebaseapp.com",
    databaseURL: "https://proyecto09-16468.firebaseio.com",
    projectId: "proyecto09-16468",
    storageBucket: "proyecto09-16468.appspot.com",
    messagingSenderId: "960293301254",
    appId: "1:960293301254:web:3d8e22c02dcc9db3ac2787",
    measurementId: "G-F0T8J83YT4"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  

  export default firebase;